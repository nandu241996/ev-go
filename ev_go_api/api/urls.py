# api/urls.py
from django.urls import path
from .views import *

urlpatterns = [
    path('register/', register, name='register'),
    path('login/', login, name='login'),
    path('forgot_password/', forgot_password, name='forgot_password'),
    path('add_device/', add_device, name='add_device'),
    path('delete_device/', delete_device, name='delete_device'),
    path('get_devices/', get_devices, name='get_devices'),
    path('start_charging/', start_charging, name='start_charging'),
    path('stop_charging/', stop_charging, name='stop_charging'),
    path('get_charging_status/', get_charging_status, name='get_charging_status'),
    path('add_amount_to_wallet/', add_amount_to_wallet, name='add_amount_to_wallet'),
    path('turn_on_device/', turn_on_device, name='turn_on_device'),
    path('turn_off_device/', turn_off_device, name='turn_off_device'),
    path('logout/', logout, name='logout'),
    path('user_profile/', user_profile, name='user_profile'),
    path('get_transaction_history/', get_transaction_history, name='get_transaction_history'),
]
