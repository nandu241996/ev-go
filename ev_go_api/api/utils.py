from datetime import datetime

def get_transaction_record(user_id, device):
    return {
        "user_id":user_id,
        "device_id":device["_id"],
        "start_time":datetime.now(),
        "end_time":"",
        "base_price":device["base_price"],
        "unit_price":device["unit_price"],
        "total_energy":"",
        "total_cost":"",
        "total_time":"",
    }