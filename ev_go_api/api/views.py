# api/views.py
import bcrypt
import logging
from django.conf import settings
from pymongo import MongoClient
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response
from datetime import datetime
from .serializers import UserSerializer, DeviceSerializer
from .authentication import CustomTokenAuthentication
from .utils import get_transaction_record
from bson import ObjectId

# client = MongoClient(settings.MONGO_DB_SETTINGS['host'], settings.MONGO_DB_SETTINGS['port'])
client = MongoClient(settings.MONGO_DB_URI)
db = client[settings.MONGO_DB_SETTINGS['db']]
users_collection = db['users']
devices_collection = db['devices']
tokens_collection = db['tokens']
transactions_collection = db['transactions']


logging.basicConfig(
    filename='ev-go.log',           # Log file name
    filemode='a',                 # Append mode ('w' for overwrite)
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG           # Log level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
)

# Suppress pymongo logs
logging.getLogger('pymongo').setLevel(logging.ERROR)

@api_view(['POST'])
def register(request):
    logging.debug('Started method register')
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        user = users_collection.find_one({"username": serializer.data['username']})
        if user:
            return Response({"message": "Duplicate username!"}, status=status.HTTP_406_NOT_ACCEPTABLE)
        hashed_password = bcrypt.hashpw(serializer.validated_data['password'].encode('utf-8'), bcrypt.gensalt())
        user_data = serializer.data
        user_data['password'] = hashed_password.decode('utf-8')  # Convert to string for storage
        user_data['wallet_amount'] = 0
        users_collection.insert_one(user_data)
        logging.debug('User registered successfully')
        return Response({"message": "User registered successfully"}, status=status.HTTP_201_CREATED)
    logging.debug('Completed method register')
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def login(request):
    logging.debug('Started method login')
    username = request.data.get('username')
    password = request.data.get('password')
    user = users_collection.find_one({"username": username})
    if user and bcrypt.checkpw(password.encode('utf-8'), user['password'].encode('utf-8')):
        token = CustomTokenAuthentication.generate_token(user['_id'])
        logging.debug('Login successful')
        return Response({"token": token, "message": "Login Successful"}, status=status.HTTP_200_OK)
    logging.debug('Completed method login')
    return Response({"message": "Invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED)

@api_view(['POST'])
@authentication_classes([CustomTokenAuthentication])
def add_device(request):
    user_id = request.user['_id']
    serializer = DeviceSerializer(data=request.data)
    if serializer.is_valid():
        device_data = serializer.data
        device_data['user_id'] = user_id  # Associate device with the user
        devices_collection.insert_one(device_data)
        return Response({"message": "Device added successfully"}, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@authentication_classes([CustomTokenAuthentication])
def delete_device(request):
    device_id = request.data['device_id']
    result = devices_collection.delete_one({"_id": ObjectId(device_id)})
    if result.deleted_count:
        return Response({"message": "Device deleted successfully"}, status=status.HTTP_200_OK)
    return Response({"message": "Device not found"}, status=status.HTTP_404_NOT_FOUND)

@api_view(['GET'])
@authentication_classes([CustomTokenAuthentication])
def get_devices(request):
    devices = list(devices_collection.find())
    for device in devices:
        device['_id'] = str(device['_id'])  # Convert ObjectId to string for JSON serialization
        device['user_id'] = str(device['user_id'])  # Convert ObjectId to string for JSON serialization
    return Response(devices, status=status.HTTP_200_OK)

@api_view(['GET'])
@authentication_classes([CustomTokenAuthentication])
def user_profile(request):
    output_json = {
        "is_success": True,
        "message":"",
        "data": ""
    }
    user_id = request.user['_id']
    user_data = users_collection.find_one({"_id": user_id})
    user_data['_id'] = str(user_data['_id'])  # Convert ObjectId to string for JSON serialization
    output_json["data"] = user_data
    return Response(output_json, status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes([CustomTokenAuthentication])
def start_charging(request):
    output_json = {
        "is_success": True,
        "message":"",
        "data": ""
    }
    device_id = request.data['device_id']
    user_id = request.user['_id']
    device = devices_collection.find_one({
        "_id": ObjectId(device_id)})
    if not device:
        output_json["message"] = "Device not found!"
        output_json["is_success"] = False
        return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE)
    elif not device["status"]:
        output_json["message"] = "Device inactive!"
        output_json["is_success"] = False
        return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE)
    elif device["is_charging"]:
        output_json["message"] = "Device already charging!"
        output_json["is_success"] = False
        return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE)
    user_data = users_collection.find_one({"_id": user_id})
    if not user_data:
        output_json["message"] = "User not found!"
        output_json["is_success"] = False
        return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE) 
    if int(user_data["wallet_amount"]) <= 0:
        output_json["message"] = "No enough money for charging!"
        output_json["is_success"] = False
        return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE)
    result = devices_collection.update_one(
        {"device_id": ObjectId(device_id)}, 
        {"$set": {
            "is_charging": True
            }
        })
    transaction_data = get_transaction_record(user_id, device)
    transaction = transactions_collection.insert_one(transaction_data)
    device['_id'] = str(device['_id'])  # Convert ObjectId to string for JSON serialization
    device['user_id'] = str(device['user_id'])  # Convert ObjectId to string for JSON serialization
    data = {
        "transaction_id":str(transaction.inserted_id),
        "device_data": device,
    }
    output_json["message"] = "Charging started successfully"
    output_json["data"] = data
    return Response(output_json, status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes([CustomTokenAuthentication])
def stop_charging(request):
    transaction_id = request.data['transaction_id']
    user_id = request.user['_id']
    transaction = transactions_collection.find_one(
        {"_id": ObjectId(transaction_id), "user_id": user_id})
    if transaction and not transaction["end_time"]:
        device = devices_collection.find_one({
            "_id": transaction['device_id']})
        stop_time = datetime.now()
        start_time = transaction['start_time'] # convert to python datetime
        total_time = stop_time - start_time
        total_time_hours = total_time.total_seconds() / 3600
        total_energy =  float(device["capacity"]) * total_time_hours
        total_cost = float(transaction["unit_price"]) * total_energy
        result = transactions_collection.update_one(
        {"_id": ObjectId(transaction_id)}, 
        {"$set": {
            "end_time": stop_time,
            "total_cost": total_cost,
            "total_energy": total_energy,
            "total_time": total_time_hours,
            }
        })
        result = devices_collection.update_one(
            {"device_id": ObjectId(transaction["device_id"])}, 
            {"$set": {
                "is_charging": False
                }
            })
        user_data = users_collection.find_one({"_id": user_id})
        balance_amount = float(user_data["wallet_amount"] - total_cost)
        result = users_collection.update_one(
            {"_id": user_id}, 
            {"$set": {
                "wallet_amount": balance_amount
                }
            })
        if result.matched_count:
            output_json = {
                "total_cost": total_cost,
                "total_energy": total_energy,
                "total_time": total_time_hours,
                "message": "Charging stopped successfully",
                "is_success": True
            }
            return Response(output_json, status=status.HTTP_200_OK)
    output_json = {
        "is_success": False,
        "message" : "No transaction found!"
    }
    return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['POST'])
@authentication_classes([CustomTokenAuthentication])
def get_charging_status(request):
    transaction_id = request.data['transaction_id']
    user_id = request.user['_id']
    transaction = transactions_collection.find_one(
        {"_id": ObjectId(transaction_id), "user_id": user_id})
    if transaction and not transaction["end_time"]:
        device = devices_collection.find_one({
            "_id": transaction['device_id']})
        current_time = datetime.now()
        start_time = transaction['start_time'] # convert to python datetime
        charged_time = current_time - start_time
        charged_time_hours = charged_time.total_seconds() / 3600
        charged_energy =  float(device["capacity"]) * charged_time_hours
        charged_cost = float(transaction["unit_price"]) * charged_energy
        result = transactions_collection.update_one(
            {"_id": ObjectId(transaction_id)}, 
            {"$set": {
                "total_cost": charged_cost,
                "total_energy": charged_energy,
                "total_time": charged_time_hours,
                }
        })
        output_json = {
            "charged_cost": charged_cost,
            "charged_energy": charged_energy,
            "charged_time": charged_time_hours,
            "message": "Charging continuing",
            "is_success": True
        }
        return Response(output_json, status=status.HTTP_200_OK)
    output_json = {
        "is_success": False,
        "message":"No transaction found!"
    }
    return Response(output_json, status=status.HTTP_406_NOT_ACCEPTABLE)

@api_view(['PUT'])
@authentication_classes([CustomTokenAuthentication])
def turn_on_device(request):
    device_id = request.data['device_id']
    result = devices_collection.update_one(
        {"device_id": ObjectId(device_id)}, 
        {"$set": {
            "status": True,
            }
        })
    if result.matched_count:
        return Response({"message": "Device turned on successfully"}, status=status.HTTP_200_OK)

@api_view(['PUT'])
@authentication_classes([CustomTokenAuthentication])
def turn_off_device(request):
    device_id = request.data['device_id']
    result = devices_collection.update_one(
        {"device_id": ObjectId(device_id)}, 
        {"$set": {
            "status": False,
            }
        })
    if result.matched_count:
        return Response({"message": "Device turned off successfully"}, status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes([CustomTokenAuthentication])
def add_amount_to_wallet(request):
    user_id = request.user['_id']
    amount = request.data.get('amount')
    if amount is None or not isinstance(amount, (int, float)):
        return Response({"message": "Invalid amount"}, status=status.HTTP_400_BAD_REQUEST)
    user_data = users_collection.find_one({"_id": user_id})
    amount = float(user_data["wallet_amount"]) + float(amount)
    result = users_collection.update_one({"_id": user_id}, {"$set": {"wallet_amount": amount}})
    if result.matched_count:
        return Response({"message": "Amount added to wallet successfully"}, status=status.HTTP_200_OK)
    return Response({"message": "User not found"}, status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
@authentication_classes([CustomTokenAuthentication])
def logout(request):
    token = request.auth
    result = tokens_collection.delete_one({"token": token})
    if result.deleted_count:
        return Response({"message": "Logout successful, token cleared"}, status=status.HTTP_200_OK)
    return Response({"message": "Invalid token"}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@authentication_classes([CustomTokenAuthentication])
def get_transaction_history(request):
    output_json = {
        "is_success": True,
        "message":"",
        "data": ""
    }
    user_id = request.user['_id']
    transactions = list(transactions_collection.find(
        {"user_id": user_id}))
    for transaction in transactions:
        transaction['_id'] = str(transaction['_id'])  # Convert ObjectId to string for JSON serialization
        transaction['user_id'] = str(transaction['user_id'])  # Convert ObjectId to string for JSON serialization
        transaction['device_id'] = str(transaction['device_id'])  # Convert ObjectId to string for JSON serialization
    output_json["data"] = transactions
    return Response(output_json, status=status.HTTP_200_OK)

@api_view(['POST'])
def forgot_password(request):
    output_json = {
        "is_success": True,
        "message":"",
        "data": ""
    }
    username = request.data.get('username')
    password = request.data.get('password')
    user_data = users_collection.find_one({"username": username})
    if user_data:
        user_id = user_data['_id']
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        hashed_password = hashed_password.decode('utf-8')  # Convert to string for storage
        result = users_collection.update_one({"_id": user_id}, {"$set": {"password": hashed_password}})
        if result.matched_count:
            output_json["message"] = "Password updated successfully."
        else:            
            output_json["is_success"] = False
            output_json["message"] = "Password not updated."
        return Response(output_json, status=status.HTTP_200_OK)
    else:
        output_json["is_success"] = False
        output_json["message"] = "User not found."
        return Response(output_json, status=status.HTTP_401_UNAUTHORIZED)